# Updates Settings

**URL** : `/api/system/firmware/updates/`

**Method** : `POST`

**Auth required** : YES

**Permissions required** : Administrator

**Data example**:
```json
{
   "check_updates_automatic":true,
   "custom_server_enable":true,
   "custom_server":"https://server.my"
}
```

## Success Response

**Code** : `200 OK`

## Error Response

**Code** : `401 UNAUTHORIZED` <br>

**Code** : `403 FORBIDDEN` <br>
**Content** : `{ "error" : "Administrator rights required" }`

