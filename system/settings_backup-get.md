# Backup settings

    Returns .json configuration file

**URL** : `/api/system/settings/backup/`

**Method** : `GET`

**Auth required** : YES

**Permissions required** : Administrator

## Success Response

**Code** : `200 OK`

## Error Response

**Code** : `401 UNAUTHORIZED` <br>
**Content** : `{ "error" : "Log in" }`

**Code** : `403 FORBIDDEN` <br>
**Content** : `{ "error" : "Administrator rights required" }`

## Notes

* just a stub. need to discus feature implementation