# Update firmware from file

**URL** : `/api/system/firmware/update/file/`

**Method** : `POST`

**Auth required** : YES

**Permissions required** : Administrator

## Success Response

**Code** : `200 OK`

**Content example**:
```json
{
    "is_latest_version_installed": true,
    "latest_version_from_file": {
        "name":"Spring Cleaning",
        "date":"04.04.2018"
        "description":"changelog in 5-15 lines"
    }
}
```
date value is 'dd.mm.yyyy'

## Error Response

**Code** : `BAD REQUEST` <br>
**Content** : `{ "error" : "Wrong firmware image" }`

**Code** : `BAD REQUEST` <br>
**Content** : `{ "error" : "Downloading problems" }`

**Code** : `401 UNAUTHORIZED` <br>
**Content** : `{ "error" : "Log in" }`

**Code** : `403 FORBIDDEN` <br>
**Content** : `{ "error" : "Administrator rights required" }`
