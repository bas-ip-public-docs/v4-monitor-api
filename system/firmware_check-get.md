# Checking new firmware at BAS-IP Server

**URL** : `/api/system/firmware/check/`

**Method** : `GET`

**Auth required** : YES

**Permissions required** : Administrator

## Success Response

**Code** : `200 OK`

**Content example**:
```json
{
    "is_latest_version_installed": true,
    "latest_version_from_cloud": {
        "name":"Spring Cleaning",
        "date":"04.04.2018",
        "description":"changelog in 5-15 lines"
    }
}
```
date value is 'dd.mm.yyyy'

## Error Response

**Code** : `401 UNAUTHORIZED` <br>
**Content** : `{ "error" : "Log in" }`

**Code** : `403 FORBIDDEN` <br>
**Content** : `{ "error" : "Administrator rights required" }`

**Code** : `503 SERVICE UNAVAILABLE` <br>
**Description** : if device's local network doesn't had internet connection.
**Content** : `{ "error" : "Does't had internet connection" }`
