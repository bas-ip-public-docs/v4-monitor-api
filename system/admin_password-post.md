# Update Admin Password

**URL** : `/api/system/password/admin/`

**Method** : `POST`

**Auth required** : YES

**Permissions required** : Administrator

**Data example**:
 ```json
 {
     "old_password": "adb123"
     "new_password": "adbd1234"
 }
 ```

## Success Response

**Code** : `200 OK`

## Error Response

**Code** : `400 BAD REQUEST` <br>
**Condition** :<br>
**Content** : `{ "error" : "Wrong password" }`


**Code:** `401 UNAUTHORIZED` <br>
**Content:** `{ "error" : "Log in" }`

**Code** : `403 FORBIDDEN` <br>
**Content** : `{ "error" : "Administrator rights required" }`

## Notes

* just a stub, needed more secure solution