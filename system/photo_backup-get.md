# Get Photo Backup

    return .zip archive with photos

**URL** : `/api/system/photo/backup/`

**Method** : `GET`

**Auth required** : YES

**Permissions required** : None

## Success Response

**Code** : `200 OK`

## Error Response

**Code** : `401 UNAUTHORIZED` <br>
**Content** : `{ "error" : "Log in" }`

## Notes

* just a stub. need to discus feature implementation