# Information about update process

**URL** : `/api/system/firmware/update/info/`

**Method** : `GET`

**Auth required** : YES

**Permissions required** : Administrator

## Success Response

**Code** : `200 OK`

**Content example**:
```json
{
    "title": "System updated"
    "name": "Spring cleaning,
    "date":"04.04.2018",
    "description":"changelog in 5-15 lines"
}
```
date value is 'dd.mm.yyyy'

## Error Response

**Code** : `401 UNAUTHORIZED` <br>
**Content** : `{ "error" : "Log in" }`

**Code** : `403 FORBIDDEN` <br>
**Content** : `{ "error" : "Administrator rights required" }`
