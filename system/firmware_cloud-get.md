# Information about update process

**URL** : `/api/system/firmware/update/progress/`

**Method** : `GET`

**Auth required** : YES

**Permissions required** : Administrator

## Success Response

**Code** : `200 OK`

**Content example**:
```json
{
    "is_updating": true,
    "progress":35
}
```
'progress' range = [0-100]
 when 0 - is updating doesn't starting and 100 - is updating finished

## Error Response

**Code** : `401 UNAUTHORIZED` <br>
**Content** : `{ "error" : "Log in" }`

**Code** : `403 FORBIDDEN` <br>
**Content** : `{ "error" : "Administrator rights required" }`

**Code** : `500 INTERNAL SERVER ERROR` <br>
**Content** : `{ "error" : "Downloading problems" }`
