# Update firmware from cloud

**URL** : `POST /api/system/firmware/update/file/start/`

**Method** : `POST`

**Auth required** : YES

**Permissions required** : Administrator

## Success Response

**Code** : `200 OK`

## Error Response

**Code:** `401 UNAUTHORIZED` <br>
**Content:** `{ "error" : "Log in" }`

**Code** : `403 FORBIDDEN` <br>
**Content** : `{ "error" : "Administrator rights required" }`
