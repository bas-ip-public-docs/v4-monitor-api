# Submit settings from file

**URL** : `/api/system/settings/file/`

**Method** : `POST`

**Auth required** : YES

**Permissions required** : Administrator

## Success Response

**Code** : `200 OK`

## Error Response

**Code** : `400 BAD REQUEST` <br>
**Content** : `{ "error" : Wrong configuration file }`

**Code:** `401 UNAUTHORIZED` <br>
**Content:** `{ "error" : "Log in" }`

**Code** : `403 FORBIDDEN` <br>
**Content** : `{ "error" : "Administrator rights required" }`

## Notes

* just a stub. need to discus feature implementation
