# Updates Settings

**URL** : `/api/system/firmware/updates/`

**Method** : `GET`

**Auth required** : YES

**Permissions required** : Administrator

## Success Response

**Code** : `200 OK`

**Content example**:
```json
{
   "check_updates_automatic":true,
   "custom_server_enable":true,
   "custom_server":"https://server.my"
}
```

## Error Response

**Code** : `401 UNAUTHORIZED` <br>

**Code** : `403 FORBIDDEN` <br>
**Content** : `{ "error" : "Administrator rights required" }`




