# Update firmware from cloud

**URL** : `POST /api/system/firmware/update/cloud/`

**Method** : `POST`

**Auth required** : YES

**Permissions required** : Administrator

## Success Response

**Code** : `200 OK`

## Error Response

**Code** : `400 BAD REQUEST` <br>
**Content** : `{ "error" : "Latest version is installed" }`

**Code:** `401 UNAUTHORIZED` <br>
**Content:** `{ "error" : "Log in" }`

**Code** : `403 FORBIDDEN` <br>
**Content** : `{ "error" : "Administrator rights required" }`

**Code** : `503 SERVICE UNAVAILABLE` <br>
**Description** : if device's local network doesn't have internet connection.
**Content** : `{ "error" : "Doesn't have internet connection" }`