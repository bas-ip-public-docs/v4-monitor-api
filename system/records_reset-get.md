# Reset records

**URL** : `/api/system/records/reset/`

**Method** : `GET`

**Auth required** : YES

**Permissions required** : None

## Success Response

**Code** : `200 OK`

## Error Response

**Code:** `401 UNAUTHORIZED` <br>
**Content:** `{ "error" : "Log in" }`