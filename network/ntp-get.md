# Custom NTP Server

**URL** : `/api/network/ntp/`

**Method** : `GET`

**Auth required** : YES

**Permissions required** : Administrator

## Success Response

**Code** : `200 OK`

**Content example**:
```json
{
  "enable":false,
  "url":"0.pool.ntp.org"
}
```

## Error Response

**Code** : `401 UNAUTHORIZED` <br>
**Content** : `{ "error" : "Log in" }`

**Code** : `403 FORBIDDEN` <br>
**Content** : `{ "error" : "Administrator rights required" }`