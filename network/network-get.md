# Network settings

**URL** : `/api/network/settings`

**Method** : `GET`

**Auth required** : YES

**Permissions required** : None

## Success Response

**Code** : `200 OK`

**Content schema**:
```json
{
    "dhcp" : [enable],
    "ip_address": "[IP ADRESS]",
    "mask" : "[IP ADRESS]",
    "gateway" : "[IP ADRESS]",
    "dns" : "[IP ADRESS]"
}
```
**Content example**:
```json
{
    "dhcp" : false,
    "ip_address": "192.168.1.96",
    "mask" : "255.255.255.0",
    "gateway" : "192.168.1.1",
    "dns" : "8.8.8.8"
}
```

## Error Response

**Code** : `401 UNAUTHORIZED` <br>
**Content** : `{ "error" : "Log in" }`

**Code** : `403 FORBIDDEN` <br>
**Content** : `{ "error" : "Administrator rights required" }`