# Update Network Settings

**URL** : `/api/network/settings/`

**Method** : `POST`

**Auth required** : YES

**Permissions required** : Administrator

**Data constraints**:
 ```json
{
    "dhcp" : [enable],
    "ip_address": "[IP ADRESS]",
    "mask" : "[IP ADRESS]",
    "gateway" : "[IP ADRESS]",
    "dns" : "[IP ADRESS]"
}
 ```

**Data example**:
 ```json
{
    "dhcp" : false,
    "ip_address": "192.168.1.96",
    "mask" : "255.255.255.0",
    "gateway" : "192.168.1.1",
    "dns" : "8.8.8.8"
}
 ```

## Success Response

**Code** : `200 OK`

## Error Response

**Code** : `400 BAD REQUEST` <br>
**Content** : `{ "error" : "ip address is invalid" }`

**Code:** `401 UNAUTHORIZED` <br>
**Content:** `{ "error" : "Log in" }`

**Code** : `403 FORBIDDEN` <br>
**Content** : `{ "error" : "Administrator rights required" }`