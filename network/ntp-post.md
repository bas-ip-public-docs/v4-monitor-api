# Update Custom NTP Server

**URL** : `/api/network/ntp/`

**Method** : `POST`

**Auth required** : YES

**Permissions required** : Administrator

**Data example**:
```json
{
  "enable":false,
  "url":"0.pool.ntp.org"
}
```

## Success Response

**Code** : `200 OK`

## Error Response

**Code** : `400 BAD REQUEST` <br>
**Content** : `{ "error" : "Wrong body" }`

**Code:** `401 UNAUTHORIZED` <br>
**Content:** `{ "error" : "Log in" }`

**Code** : `403 FORBIDDEN` <br>
**Content** : `{ "error" : "Administrator rights required" }`