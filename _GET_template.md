# Title

    Description

**URL** : `/api/__/`

**Method** : `GET`

**Auth required** : YES

**Permissions required** : None

## Success Response

**Code** : `200 OK`

**Content schema**:
```json
{
    "first_key": "[value]",
    "second_key": "[value]"
}
```
**Content example**:
```json
{
    "current_language": "English",
    "all_supported_languages": [
        "English",
        "Russian"
        ]
}
```

## Error Response

**Code** : `XXX ERROR` <br>
**Content** : `{ "error" : "[Message]" }`


**Code** : `401 UNAUTHORIZED` <br>
**Content** : `{ "error" : "Log in" }`

## Notes

* delete this section if doesn't needed
