# Login

    Used to collect a Token for a registered User.

**URL** : `/api/login/`

**Method** : `POST`

**Auth required** : NO

**Data constraints**:
```json
{
    "username": "[valid username]",
    "password": "[password in plain text]"
}
```

**Data example**:
```json
{
    "username": "admin",
    "password": "abcd1234"
}
```

## Success Response

**Code** : `200 OK`

**Content example**:
```json
{
    "token" : "93144b288eb1fdccbe46d6fc0f241a51766ecd3d",
    "account_type" : "[user|administrator]"
}
```

## Error Response

**Code** : `400 BAD REQUEST` <br>
**Condition** : If 'username' and 'password' combination is wrong.<br>
**Content:** `{ "error" : "Wrong login or password" }`

## Notes

* just an example for stub, probably will be change in future
