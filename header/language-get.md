# Current Language

    Get current system language.

**URL** : `/api/language/`

**Method** : `GET`

**Auth required** : YES

**Permissions required** : None

## Success Response

**Code** : `200 OK`

**Content example**:
```json
{
    "current_language": "English",
    "all_supported_languages": [
        "English",
        "Russian"
        ]
}
```

## Error Response

**Code** : `401 UNAUTHORIZED` <br>
**Content** : `{ error : "Log in" }`

## Notes

* Number of languages might increase in the future.