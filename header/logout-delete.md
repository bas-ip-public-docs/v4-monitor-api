# Logout

    Used to collect a Token for a registered User.

**URL** : `/api/logout/`

**Method** : `DELETE`

**Auth required** : YES

**Permissions required** : None

**Data constraints**:
```json
{
    "token": "[valid token]"
}
```

**Data example**:
```json
{
    "token": "93144b288eb1fdccbe46d6fc0f241a51766ecd3d"
}
```

## Success Response

**Code** : `200 OK`

## Error Response

**Code** : `400 BAD REQUEST` <br>
**Condition** : If token isn't in sessions list. <br>
**Content** : `{ "error" : "Wrong token" }`

**Code:** `401 UNAUTHORIZED` <br>
**Content:** `{ "error" : "Log in" }`

## Notes

* See [LogIn](login0post.md) notes