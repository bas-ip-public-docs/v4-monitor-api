# Update Language

    Uses for setting web and system language

**URL** : `/api/language/`

**Method** : `POST`

**Auth required** : YES

**Permissions required** : None

**Data description**: <br>
Value must to be one from 'all_supported_languages' array (See [GET](language-get.md) method).

```json
{
    "language": "[valid language]"
}
```

**Data example**

```json
{
    "language": "English"
}
```

## Success Response

**Code** : `200 OK`

## Error Response

**Code** : `400 BAD REQUEST` <br>
**Condition** : If 'language' value is wrong. <br>
**Content:** `{ "error" : "Wrong language" }`

**Code:** `401 UNAUTHORIZED` <br>
**Content:** `{ "error" : "Log in" }`