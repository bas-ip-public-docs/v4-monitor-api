# SIP Status

    Get current SIP status.

**URL** : `/api/sip/status/`

**Method** : `GET`

**Auth required** : YES

**Permissions required** : None

## Success Response

**Code** : `200 OK`

**Content schema**:
```json
{
    "sip_status": "[OK|OFFLINE|ERROR]"
}
```
Available statuses: 
* `OK` - SIP is working
* `OFFLINE` - SIP is disabled 
* `ERROR` - SIP is enabled, but registration is failure

**Content example**:
```json
{
    "sip_status": "OK"
}
```

## Error Response

**Code** : `401 UNAUTHORIZED` <br>
**Content** : `{ "error" : "Log in" }`