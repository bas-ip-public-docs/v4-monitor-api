# Advertisement Settings

**URL** : `/api/advanced/announcement/`

**Method** : `GET`

**Auth required** : YES

**Permissions required** : Administrator

## Success Response

**Code** : `200 OK`

**Content example**:
```json
{
    "enable": false,
    "url": "http://example.com",
    "timeout": 300
}
```

## Error Response

**Code** : `401 UNAUTHORIZED` <br>
**Content** : `{ "error" : "Log in" }`

**Code** : `403 FORBIDDEN` <br>
**Content** : `{ "error" : "Administrator rights required" }`