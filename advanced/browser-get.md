# Browser Settings

**URL** : `/api/advanced/browser/`

**Method** : `GET`

**Auth required** : YES

**Permissions required** : None

## Success Response

**Code** : `200 OK`

**Content example**:
```json
{
    "main": "http://example.com",
    "mall": "http://example.com",
    "stock": "http://example.com",
    "cook": "http://example.com",
    "map": "http://example.com"
}
```

## Error Response

**Code** : `401 UNAUTHORIZED` <br>
**Content** : `{ "error" : "Log in" }`