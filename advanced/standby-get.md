# Standby Mode

    Standby mode represents splash screen when device is inactive.
    If stanby mode is enabled, we hava a 3 options:
        ip_cam_mode
        photo_frame
        clock
    First mode also had 3 options
        single
        tour
        quad_splitter
    All of this suboptions have his own settings, and also photoframe have options.

**URL** : `/api/advanced/standby/`

**Method** : `GET`

**Auth required** : YES

**Permissions required** : None

## Success Response

**Code** : `200 OK`

**Content example**:
```json
{
  "enable": false,
  "mode": "ip_cam_mode",
  "ip_cam_mode": {
    "mode": "single",
    "single": {
      "name": "name",
      "url": "rtsp://"
    },
    "tour": {
      "ip_cams": [
        {
          "name": "name",
          "url": "rtsp://"
        },
        {
          "name": "name",
          "url": "rtsp://"
        },
        {
          "name": "name",
          "url": "rtsp://"
        }
      ],
      "timeout": 60
    },
    "quad_splitter": {
      "ip_cam_1": {
        "name": "name",
        "url": "rtsp://"
      },
      "ip_cam_2": {
        "name": "name",
        "url": "rtsp://"
      },
      "ip_cam_3": {
        "name": "name",
        "url": "rtsp://"
      },
      "ip_cam_4": {
        "name": "name",
        "url": "rtsp://"
      }
    }
  },
  "photo_frame": {
    "directory_path": "/mnt/sdcard/photo",
    "clock_enable": false
  }
}
```

## Error Response

**Code** : `401 UNAUTHORIZED` <br>
**Content** : `{ "error" : "Log in" }`