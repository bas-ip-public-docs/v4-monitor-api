# Update Browser Settings

**URL** : `/api/advanced/browser/`

**Method** : `POST`

**Auth required** : YES

**Permissions required** : None

**Data example**:
```json
{
    "main": "http://example.com",
    "mall": "http://example.com",
    "stock": "http://example.com",
    "cook": "http://example.com",
    "map": "http://example.com"
}
```

## Success Response

**Code** : `200 OK`

## Error Response

**Code** : `400 BAD REQUEST` <br>
**Content** : `{ "error" : "Wrong body" }`

**Code:** `401 UNAUTHORIZED` <br>
**Content:** `{ "error" : "Log in" }`