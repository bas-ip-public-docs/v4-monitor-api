# Update Standby Mode

**URL** : `/api/advanced/standby/`

**Method** : `POST`

**Auth required** : YES

**Permissions required** : None

**Data example**:
 ```json
{
  "enable": false,
  "mode": "ip_cam_mode",
  "ip_cam_mode": {
    "mode": "single",
    "single": {
      "name": "name",
      "url": "rtsp://"
    },
    "tour": {
      "ip_cams": [
        {
          "name": "name",
          "url": "rtsp://"
        },
        {
          "name": "name",
          "url": "rtsp://"
        },
        {
          "name": "name",
          "url": "rtsp://"
        }
      ],
      "timeout": 60
    },
    "quad_splitter": {
      "ip_cam_1": {
        "name": "name",
        "url": "rtsp://"
      },
      "ip_cam_2": {
        "name": "name",
        "url": "rtsp://"
      },
      "ip_cam_3": {
        "name": "name",
        "url": "rtsp://"
      },
      "ip_cam_4": {
        "name": "name",
        "url": "rtsp://"
      }
    }
  },
  "photo_frame": {
    "directory_path": "/mnt/sdcard/photo",
    "clock_enable": false
  }
}
 ```

'mode' = 'ip_cam_mode'|'photo_frame'|'clock'
'ip_cam_mode'::'mode' = 'single'|'tour'|'quad_splitter'

## Success Response

**Code** : `200 OK`

## Error Response

**Code** : `400 BAD REQUEST` <br>
**Content** : `{ "error" : "Wrong mode" }`<br>
`{ "error" : "Wrong ip_cam_mode" }`<br>
`{ "error" : "Wrong body" }`<br>


**Code:** `401 UNAUTHORIZED` <br>
**Content:** `{ "error" : "Log in" }`