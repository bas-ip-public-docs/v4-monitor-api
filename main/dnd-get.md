# DND Setting

    Return device's DND settings.

**URL** : `/api/main/dnd/`

**Method** : `GET`

**Auth required** : YES

**Permissions required** : None

## Success Response

**Code** : `200 OK`

**Content schema**:
```json
{
  "enable_silent_mode_schedule":[boolean],
  "enable_dnd_mode_schedule":[boolean],
  "dnd_time_from":"[XX:XX]",
  "dnd_time_to":"[XX:XX]",
  "silence_time_from":"[XX:XX]",
  "silence_time_to":"[XX:XX]"
}
```
where [XX:XX] - time in 24-hour format

**Content example**:
```json
{
  "enable_silent_mode_schedule":false,
  "enable_dnd_mode_schedule":false,
  "dnd_time_from":"22:00",
  "dnd_time_to":"07:00",
  "silence_time_from":"22:00",
  "silence_time_to":"07:00"
}
```

## Error Response

**Code** : `401 UNAUTHORIZED` <br>
**Content** : `{ "error" : "Log in" }`
