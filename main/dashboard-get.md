# Dashboard Information

    Return dashboard information

**URL** : `/api/main/dashboard/`

**Method** : `GET`

**Auth required** : YES

**Permissions required** : None

## Success Response

**Code** : `200 OK`

**Content schema**:
```json
{
    "framework_status": "[valid status information]",
    "web_server_status": "[valid status information]",
    "launcher_status": "[valid status information]",
    "voip_status": "[valid status information]",
    "security_status": "[valid status information]",
    "smart_status": "[valid status information]",
    "services_status": "[valid status information]",
    "quad_splitter_status": "[valid status information]"
}
```
If something wrong status will be `ERROR`

## Error Response

**Code** : `401 UNAUTHORIZED` <br>
**Content** : `{ "error" : "Log in" }`

## Notes

* JSON answer is just a stub, need to discus what we want to see in dashboard