# Update DND Settings

**URL** : `/api/main/dnd/`

**Method** : `POST`

**Auth required** : YES

**Permissions required** : None

**Data constraints**:
```json
{
  "enable_silent_mode_schedule":[boolean],
  "enable_dnd_mode_schedule":[boolean],
  "dnd_time_from":"[XX:XX]",
  "dnd_time_to":"[XX:XX]",
  "silence_time_from":"[XX:XX]",
  "silence_time_to":"[XX:XX]"
}
```
where [XX:XX] - time in 24-hour format

**Data example**:
```json
{
  "enable_silent_mode_schedule":false,
  "enable_dnd_mode_schedule":false,
  "dnd_time_from":"22:00",
  "dnd_time_to":"07:00",
  "silence_time_from":"22:00",
  "silence_time_to":"07:00"
}
```

## Success Response

**Code** : `200 OK`

## Error Response

**Code** : `400  BAD REQUEST` <br>
**Content** : `{ "error" : "JSON body is invalid" }`


**Code:** `401 UNAUTHORIZED` <br>
**Content:** `{ "error" : "Log in" }`