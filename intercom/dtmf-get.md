# DTMF Settings

    First key is always avaiable, second key might be disabled.
    First key had value by default. If 'default_value' is disabled,
    key uses data from 'value' field.

**URL** : `/api/intercom/dtmf/`

**Method** : `GET`

**Auth required** : YES

**Permissions required** : None

## Success Response

**Code** : `200 OK`

**Content example**:
```json
{
  "first_key":{
    "name":"key #1",
    "default_value":true,
    "value":"#"
  },
  "second_key":{
    "enabled":false,
    "name":"key #2",
    "value":"0"
  }
}
```

## Error Response

**Code** : `401 UNAUTHORIZED` <br>
**Content** : `{ "error" : "Log in" }`
