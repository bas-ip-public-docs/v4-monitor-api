# Extended Phone Settings

**URL** : `/api/intercom/exphone/`

**Method** : `GET`

**Auth required** : YES

**Permissions required** : None

## Success Response

**Code** : `200 OK`

**Content example**:
```json
{
    "max_phones": 4,
    "list": [
        "sip:101@sip.bas-ip.com"
        "sip:102@sip.bas-ip.com"
        "sip:103@sip.bas-ip.com"
        "sip:104@sip.bas-ip.com"
    ]
}
```
'list' might be empty, max array size - in 'max_phones' field.

## Error Response

**Code** : `401 UNAUTHORIZED` <br>
**Content** : `{ "error" : "Log in" }`