# Advanced options

    'camera' - on/off video in calls
    'auto_pickup' - aumatic pick up calls
    'auto_answer' - play "nobody home" message and record message if nobody answers
     after 30s of incoming call
    'auto_hung_up_after_dtmf' - automatic end call after opening door
    'hooter' - on/off hooter(ext. speaker) and timeout after wich starting work hooter
    when an incoming call is started

**URL** : `/api/intercom/advanced/`

**Method** : `GET`

**Auth required** : YES

**Permissions required** : None

## Success Response

**Code** : `200 OK`

**Content example**:
```json
{
  "camera": false,
  "auto_pickup": false,
  "auto_answer": false,
  "auto_hung_up_after_dtmf": false,
  "hooter": {
    "enable": false,
    "timeout": 10
  }
}
```

## Error Response

**Code** : `401 UNAUTHORIZED` <br>
**Content** : `{ "error" : "Log in" }`