# SOS Button

**URL** : `/api/intercom/sos/`

**Method** : `GET`

**Auth required** : YES

**Permissions required** : None

## Success Response

**Code** : `200 OK`

**Content example**:
```json
{
    "sos_button": "101@sip.bas-ip.com",
}
```

## Error Response

**Code** : `401 UNAUTHORIZED` <br>
**Content** : `{ "error" : "Log in" }`