# Update Logical Address

    See description of [GET](room-get.md) method

**URL** : `/api/intercom/room/`

**Method** : `POST`

**Auth required** : YES

**Permissions required** : Administrator

**Data constraints**:
numbers must be in specified range, 'sync' must be a string with 6 numbers
```json
{
  "building": [1-999],
  "unit": [1-99],
  "floor": [0-98],
  "family": [0-99],
  "device_code": [0-9],
  "sync": "[String, only numbers. 0 < Length < 16]",
  "server_ms": "[IP addr/DNS]",
  "password": "[not empty]",
}
```

**Data example**:
```json
{
  "building": 1,
  "unit": 1,
  "floor": 1,
  "family": 11,
  "device_code": 0,
  "sync": "123456",
  "server_ms": "192.168.1.75",
  "password": "123456",
}
```

## Success Response

**Code** : `200 OK`

## Error Response

**Code** : `400 BAD REQUEST` <br>
**Content** :<br>
`{
	"errors": [{
		"message": "Wrong server_ms value",
		"field": "server_ms"
	}, {
		"message": "Wrong unit number",
		"field": "unit"
	}, {
		"message": "Wrong building number",
		"field": "building"
	}, {
		"message": "Wrong device_code number",
		"field": "device_code"
	}, {
		"message": "Wrong floor number",
		"field": "floor"
	}, {
		"message": "Wrong sync value",
		"field": "sync"
	}, {
		"message": "Wrong family number",
		"field": "family"
	}, {
		"message": "Wrong password value",
		"field": "password"
	}]
}`

**Code:** `401 UNAUTHORIZED` <br>
**Content:** `{ "error" : "Log in" }`

**Code** : `403 FORBIDDEN` <br>
**Content** : `{ "error" : "Administrator rights required" }`
