# Update Extended Phone Settings

**URL** : `/api/intercom/exphone/`

**Method** : `POST`

**Auth required** : YES

**Permissions required** : None

**Data example**:
```json
{
    "list": [
        "sip:101@sip.bas-ip.com"
    ]
}
```

## Success Response

**Code** : `200 OK`

## Error Response

**Code** : `400 BAD REQUEST` <br>
**Description** If in 'list' array size > 'max_phones' from GET request
**Content** : `{ "error" : "To many phones"}

**Code:** `401 UNAUTHORIZED` <br>
**Content:** `{ "error" : "Log in" }`
