# Logical address information

    Logical adress is meta information about device for BAS-IP system.
    Adress consist numbers of building, unit, floor, appartament and device's number
    in appartament. If device_code is 0 - this device is master device, and if device
    code [1-10], that a slave device. Some settings sync between devices and you can
    change setting only if you work with master device

**URL** : `/api/intercom/room/`

**Method** : `GET`

**Auth required** : YES

**Permissions required** : Administrator

## Success Response

**Code** : `200 OK`

**Content example**:
numbers must be in specified range, 'sync' must be a string with 6 numbers
```json
{
  "building": 1,
  "unit": 1,
  "floor": 1,
  "family": 11,
  "device_code": 0,
  "sync": "123456",
  "server_ms": "192.168.1.75",
  "password": "123456",
}
```

## Error Response

**Code** : `401 UNAUTHORIZED` <br>
**Content** : `{ "error" : "Log in" }`

**Code** : `403 FORBIDDEN` <br>
**Content** : `{ "error" : "Administrator rights required" }`
