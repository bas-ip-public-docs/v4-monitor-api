# Update SIP Settings

**URL** : `/api/intercom/sip/`

**Method** : `POST`

**Auth required** : YES

**Permissions required** : Administrator

**Data example**:
 ```json
{
  "enable": false,
  "proxy": "sip:192.168.1.99",
  "realm": "192.168.1.99",
  "user": "105",
  "password": "abc123",
  "timeout":[120|300|600|1200|1800] //one of these values
  "stun": {
    "ip": "192.168.1.99",
    "port": 5060
  }
}
 ```

## Success Response

**Code** : `200 OK`

## Error Response

**Code:** `400 BAD REQUEST` <br>
**Content:** `{ "error" : "Wrong body" }`

**Code:** `401 UNAUTHORIZED` <br>
**Content:** `{ "error" : "Log in" }`

**Code** : `403 FORBIDDEN` <br>
**Content** : `{ "error" : "Administrator rights required" }`