# Update SOS Button

**URL** : `/api/intercom/sos/`

**Method** : `POST`

**Auth required** : YES

**Permissions required** : None

**Data example**:
 ```json
{
    "sos_button": "101@sip.bas-ip.com",
}
 ```

## Success Response

**Code** : `200 OK`

## Error Response

**Code** : `400 BAD REQUEST` <br>
**Content** : `{ "error" : "Wrong body" }`

**Code:** `401 UNAUTHORIZED` <br>
**Content:** `{ "error" : "Log in" }`
