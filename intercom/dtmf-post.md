# Update DTMF settings

**URL** : `/api/intercom/dtmf`

**Method** : `POST`

**Auth required** : YES

**Permissions required** : None

**Data example**:
 ```json
{
  "first_key":{
    "name":"key #1",
    "default_value":true,
    "value":"#"
  },
  "second_key":{
    "enabled":false,
    "name":"key #2",
    "value":"0"
  }
}
 ```
'value' must be a DTMF code (^[0-9#\*]+$ - regex)

## Success Response

**Code** : `200 OK`

## Error Response

**Code** : `400 BAD REQUEST` <br>
**Content** : `{ "error" : "Wrong DTMF value" }`


**Code:** `401 UNAUTHORIZED` <br>
**Content:** `{ "error" : "Log in" }`
