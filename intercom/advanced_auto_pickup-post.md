# Update Auto Pickup Value

**URL** : `/api/intercom/advanced/autopickup/`

**Method** : `POST`

**Auth required** : YES

**Permissions required** : None

**Data constraints**:
 ```json
 {
     "auto_pickup": [boolean]
 }
 ```

## Success Response

**Code** : `200 OK`

## Error Response

**Code** : `400 BAD REQUEST` <br>
**Content** :`{ "error" : "Wrong body" }`

**Code:** `401 UNAUTHORIZED` <br>
**Content:** `{ "error" : "Log in" }`