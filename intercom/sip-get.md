# SIP Settings

    Represents device's SIP settings.

**URL** : `/api/intercom/sip/`

**Method** : `GET`

**Auth required** : YES

**Permissions required** : Administrator

## Success Response

**Code** : `200 OK`

**Content example**:
```json
{
  "enable": false,
  "proxy": "sip:192.168.1.99",
  "realm": "192.168.1.99",
  "user": "105",
  "password": "abc123",
  "timeout":[120|300|600|1200|1800] //one of these values
  "stun": {
    "ip": "192.168.1.99",
    "port": 5060
  }
}
```

## Error Response

**Code** : `401 UNAUTHORIZED` <br>
**Content** : `{ "error" : "Log in" }`

**Code** : `403 FORBIDDEN` <br>
**Content** : `{ "error" : "Administrator rights required" }`
