# Update Hooter Settings

**URL** : `/api/intercom/advanced/hooter/`

**Method** : `POST`

**Auth required** : YES

**Permissions required** : None

**Data constraints**:
 ```json
 {
     "hooter": {
         "enable": false,
         "timeout": [0-60]
       }
 }
 ```

## Success Response

**Code** : `200 OK`

## Error Response

**Code** : `400 BAD REQUEST` <br>
**Content** :`{ "error" : "Wrong body" }`

**Code:** `401 UNAUTHORIZED` <br>
**Content:** `{ "error" : "Log in" }`