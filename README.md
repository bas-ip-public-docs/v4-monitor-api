## Open Endpoints

Open endpoints require no Authentication.

* [X] [Login](header/login-post.md) : `POST /api/login/`

## Endpoints that require Authentication

Closed endpoints require a valid Token to be included in the header of the
request. A Token can be acquired from the Login view above.

Also, we had two types of accounts: user account and admin account. 
Some of methods request ADMIN permission.

### Current User related

* [X] [Logout](header/logout-delete.md) : `DELETE /api/logout/`

### Top Bar related

* [X] [Current Language](header/language-get.md) : `GET /api/language/`
* [X] [Set Language](header/language-post.md) : `POST /api/language/`
* [X] [SIP Status](header/sip_status-get.md) : `GET /api/sip/status/`

### Main page related

* [X] [Dashboard](main/dashboard-get.md) : `GET /api/main/dashboard/`
* [X] [DND settings](main/dnd-get.md) : `GET /api/main/dnd/`
* [X] [Update DND](main/dnd-post.md) : `POST /api/main/dnd/`

### Intercom page related

* [X] [Logical Address settings](intercom/room-get.md) : `GET /api/intercom/room/`
* [X] [Update Logical Address](intercom/room-post.md) : `POST /api/intercom/room/`
* [X] [SIP settings](intercom/sip-get.md) : `GET /api/intercom/sip/`
* [X] [Update SIP settings](intercom/sip-post.md) : `POST /api/intercom/sip/`
* [X] [DTMF settings](intercom/dtmf-get.md) : `GET /api/intercom/dtmf/`
* [X] [Update DTMF settings](intercom/dtmf-post.md) : `POST /api/intercom/dtmf/`
* [X] [Advanced Intercom settings](intercom/advanced-get.md) : `GET /api/intercom/advanced/`
* [X] [Update Advanced Camera value](intercom/advanced_camera-post.md) : `POST /api/intercom/advanced/camera/`
* [X] [Update Advanced Auto Pickup value](intercom/advanced_auto_pickup-post.md) : `POST /api/intercom/advanced/autopickup/`
* [X] [Update Advanced Auto Answer Value](intercom/advanced_auto_answer-post.md) : `POST /api/intercom/advanced/autoanswer/`
* [X] [Update Advanced Auto HungUp Value](intercom/advanced_auto_hung_up-post.md) : `POST /api/intercom/advanced/autohungup/`
* [X] [Update Advanced Hooter Setting](intercom/advanced_hooter-post.md) : `POST /api/intercom/advanced/hooter/`
* [X] [Ex-Phone settings](intercom/ex_phone-get.md) : `GET /api/intercom/exphone/`
* [X] [Update Ex-Phone](intercom/ex_phone-post.md) : `POST /api/intercom/exphone/`
* [X] [SOS Button settings](intercom/sos_button-post.md) : `GET /api/intercom/sos/`
* [X] [Update SOS Button](intercom/sos_button-get.md) : `POST /api/intercom/sos/`

### Network related

* [X] [Network settings](network/network-get.md) : `GET /api/network/settings/`
* [X] [Update Network settings](network/network-post.md) : `POST /api/network/settings/`
* [X] [Custom NTP Server](network/ntp-get.md) : `GET /api/network/ntp/`
* [X] [Update Custom NTP Server](network/ntp-post.md) : `POST /api/network/ntp/`

### Security related

* [X] [IP Cameras](security/ip_cameras-get.md) : `GET /api/security/ipcam/`
* [X] [Update IP Cameras](security/ip_cameras-post.md) : `POST /api/security/ipcam/`
* [X] [Now IP settings](security/now_ip-get.md) : `GET /api/security/nowip/`
* [X] [Update Now IP](security/now_ip-post.md) : `POST /api/security/nowip/`
* [X] [Scene settings](security/scene-get.md) : `GET /api/security/scene/`
* [X] [Update scene settings](security/scene-post.md) : `POST /api/security/scene/`
* [X] [Zone settings](security/zone-get.md) : `GET /api/security/zone/`
* [X] [Update zone settings](security/zone-post.md) : `POST /api/security/zone/`

### Advanced related

* [X] [Announcement](advanced/announcement-get.md) : `GET /api/advanced/announcement/`
* [X] [Update announcement](advanced/announcement-post.md) : `POST /api/advanced/announcement/`
* [X] [Run announcement](advanced/announcement-run-get.md) : `GET /api/advanced/announcement/run/`
* [X] [Browser links](advanced/browser-get.md) : `GET /api/advanced/browser/`
* [X] [Update browser links](advanced/browser-post.md) : `POST /api/advanced/browser/`
* [X] [Standby settings](advanced/standby-get.md) : `GET /api/advanced/standby/`
* [X] [Update standby settings](advanced/standby-post.md) : `POST /api/advanced/standby/`

### System related

* [X] [Set new admin password](system/admin_password-post.md) : `POST /api/system/password/admin/`
* [X] [Set new user password](system/user_password-post.md) : `POST /api/system/password/user/`
* [X] [Reboot device](system/reboot-get.md) : `GET /api/system/reboot/`
* [X] [Submit settings from file](system/settings_from_file-post.md) : `POST /api/system/settings/file/`
* [X] [Backup settings](system/settings_backup-get.md) : `GET /api/system/settings/backup/`
* [X] [Backup photo](system/photo_backup-get.md) : `GET /api/system/photo/backup/`
* [X] [Reset all settings to default](system/settings_default-get.md) : `GET /api/system/settings/default/`
* [X] [Reset records](system/records_reset-get.md) : `GET /api/system/records/reset/`
* [X] [Update firmware](system/firmware_file-post.md) : `POST /api/system/firmware/update/file/`
* [X] [Check new firmware from cloud](system/firmware_check-get.md) : `GET /api/system/firmware/check/`
* [X] [Information about update process](system/firmware_cloud-get.md) : `GET /api/system/firmware/update/progress/`
* [X] [Update firmware from cloud](system/firmware_cloud-post.md) : `POST /api/system/firmware/update/cloud/`

