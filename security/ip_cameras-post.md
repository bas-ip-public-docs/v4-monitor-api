# Update IP Cameras Settings

**URL** : `/api/security/ipcam/`

**Method** : `POST`

**Auth required** : YES

**Permissions required** : None

**Data example**:
 ```json
{
  "max": 32,
  "items": [
    {
      "name": "gate 1",
      "url": "rtsp://ipcam-adress.com/1"
    },
    {
      "name": "gate 2",
      "url": "rtsp://ipcam-adress.com/2"
    },
    {
      "name": "gate 3",
      "url": "rtsp://ipcam-adress.com/3"
    }
  ]
}
 ```

## Success Response

**Code** : `200 OK`

## Error Response

**Code** : `400 BAD REQUEST` <br>
**Content** : `{ "error" : "Exceeded the maximum count of IP Cameras" }`
**Content** : `{ "error" : "Camera can't have empty fields" }`

**Code:** `401 UNAUTHORIZED` <br>
**Content:** `{ "error" : "Log in" }`