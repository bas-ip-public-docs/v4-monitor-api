# Update Scene Settings

**URL** : `/api/security/scene/`

**Method** : `POST`

**Auth required** : YES

**Permissions required** : None

## Success Response

**Code** : `200 OK`

**Content example**:
```json
{
  "zones":[
      {
          "zone_id":0,
          "scene_out":false,
          "scene_home":false,
          "scene_sleep":false
      },
      {
          "zone_id":1,
          "scene_out":false,
          "scene_home":false,
          "scene_sleep":false
      },
      {
          "zone_id":2,
          "scene_out":false,
          "scene_home":false,
          "scene_sleep":false
      },
      {
          "zone_id":3,
          "scene_out":false,
          "scene_home":false,
          "scene_sleep":false
      },
      {
          "zone_id":4,
          "scene_out":false,
          "scene_home":false,
          "scene_sleep":false
      },
      {
          "zone_id":5,
          "scene_out":false,
          "scene_home":false,
          "scene_sleep":false
      },
      {
          "zone_id":6,
          "scene_out":false,
          "scene_home":false,
          "scene_sleep":false
      },
      {
          "zone_id":7,
          "scene_out":false,
          "scene_home":false,
          "scene_sleep":false
      }
    ]
}
```
'zone_id' range is [0-7]

## Error Response

**Code** : `400 BAD REQUEST` <br>
**Content** : `{ "error" : "Wrong zone_id" }`


**Code:** `401 UNAUTHORIZED` <br>
**Content:** `{ "error" : "Log in" }`

## Notes

* delete this section if doesn't needed