# Zone Settings

**URL** : `/api/security/zone/`

**Method** : `GET`

**Auth required** : YES

**Permissions required** : None

## Success Response

**Code** : `200 OK`

**Content example**:
```json
{
    "types":["normal","emergency","24_hour"],
    "modes":["3c","no","nc","bell"],
    "delays":[0,5,15,20,25,40,60],
    "sensors":["smoke","gas","pir","door","window","panic","flood","pull_cord","bed_mat"],
    "zones":[
        {
            "zone_id":0,
            "type":"normal"
            "mode":"no",
            "delay":15,
            "sensor":"smoke"
        },
        {
            "zone_id":1,
            "type":"normal"
            "mode":"no",
            "delay":15,
            "sensor":"smoke"
        },
        {
            "zone_id":2,
            "type":"normal"
            "mode":"no",
            "delay":15,
            "sensor":"smoke"
        },
        {
            "zone_id":3,
            "type":"normal"
            "mode":"no",
            "delay":15,
            "sensor":"smoke"
        },
        {
            "zone_id":4,
            "type":"normal"
            "mode":"no",
            "delay":15,
            "sensor":"smoke"
        },
        {
            "zone_id":5,
            "type":"normal"
            "mode":"no",
            "delay":15,
            "sensor":"smoke"
        },
        {
            "zone_id":6,
            "type":"normal"
            "mode":"no",
            "delay":15,
            "sensor":"smoke"
        },
        {
            "zone_id":7,
            "type":"normal"
            "mode":"no",
            "delay":15,
            "sensor":"smoke"
        }
    ]
}
```

## Error Response

**Code** : `401 UNAUTHORIZED` <br>
**Content** : `{ "error" : "Log in" }`