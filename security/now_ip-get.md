# NowIP Settings

    Description

**URL** : `/api/security/nowip/`

**Method** : `GET`

**Auth required** : YES

**Permissions required** : None

## Success Response

**Code** : `200 OK`

**Content example**:
```json
{
    "now_ip" : {
        "enable":false,
        "main_number":"911@sip.bas-ip.com",
        "second_number":"911@sip.bas-ip.com",
        "second_number_delay_call":20
    },
    "auto_call" : {
        "enable": false,
        "number": "sip:911@192.168.12.40"
    },
    "heartbeat" : {
        "enable": false,
        "data": "message string",
        "timeout": 30
    },
    "zones":[
        {
            "zone_id":0,
            "zone_name":"door sensor"
        },
        {
            "zone_id":1,
            "zone_name":"gate sensor"
        },
        {
            "zone_id":2,
            "zone_name":"smoke sensor"
        },
        {
            "zone_id":8,
            "zone_name":"motion detector"
        },
        {
            "zone_id":15,
            "zone_name":"window sensor"
        }
    ]
}
```
'zone_id' range is [0-15]

## Error Response

**Code** : `401 UNAUTHORIZED` <br>
**Content** : `{ "error" : "Log in" }`