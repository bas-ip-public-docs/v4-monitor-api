# Update Zone Settings

    Delay might be changed only if type of zone is 'normal', else delay = 0

**URL** : `/api/security/zone/`

**Method** : `POST`

**Auth required** : YES

**Permissions required** : None

**Data example**:
 ```json
{
    "zones":[
        {
            "zone_id":0,
            "type":"normal"
            "mode":"NO",
            "delay":15,
            "sensor":"Smoke"
        },
        {
            "zone_id":1,
            "type":"normal"
            "mode":"NO",
            "delay":15,
            "sensor":"Smoke"
        },
        {
            "zone_id":2,
            "type":"normal"
            "mode":"NO",
            "delay":15,
            "sensor":"Smoke"
        },
        {
            "zone_id":3,
            "type":"normal"
            "mode":"NO",
            "delay":15,
            "sensor":"Smoke"
        },
        {
            "zone_id":4,
            "type":"normal"
            "mode":"NO",
            "delay":15,
            "sensor":"Smoke"
        },
        {
            "zone_id":5,
            "type":"normal"
            "mode":"NO",
            "delay":15,
            "sensor":"Smoke"
        },
        {
            "zone_id":6,
            "type":"normal"
            "mode":"NO",
            "delay":15,
            "sensor":"Smoke"
        },
        {
            "zone_id":7,
            "type":"normal"
            "mode":"NO",
            "delay":15,
            "sensor":"Smoke"
        }
    ]
}
 ```
All values of 'type', 'mode', 'delay', 'sensor' must be one of values from arrays from GET request

## Success Response

**Code** : `200 OK`

## Error Response

**Code** : `400 BAD REQUEST` <br>
**Content** :<br>
`{ "error" : "Wrong type value" }`<br>
`{ "error" : "Wrong mode value" }`<br>
`{ "error" : "Wrong delay value" }`<br>
`{ "error" : "Wrong sensor value" }`<br>
`{ "error" : "Wrong zone_id" }`<br>


**Code:** `401 UNAUTHORIZED` <br>
**Content:** `{ "error" : "Log in" }`