# IP Cameras Settings

**URL** : `/api/security/ipcam/`

**Method** : `GET`

**Auth required** : YES

**Permissions required** : None

## Success Response

**Code** : `200 OK`

**Content example**:
```json
{
  "max": 32,
  "items": [
    {
      "name": "gate 1",
      "url": "rtsp://ipcam-adress.com/1"
    },
    {
      "name": "gate 2",
      "url": "rtsp://ipcam-adress.com/2"
    },
    {
      "name": "gate 3",
      "url": "rtsp://ipcam-adress.com/3"
    }
  ]
}
```
'items' array might be empty. max array size = 'max' field

## Error Response

**Code** : `401 UNAUTHORIZED` <br>
**Content** : `{ "error" : "Log in" }`