# Title

    Description

**URL** : `/api/__/`

**Method** : `POST`

**Auth required** : YES

**Permissions required** : None

**Data description**: <br>
_Data description if needed_

**Data constraints**:
 ```json
 {
     "key": "[valid value]"
 }
 ```

**Data example**:
 ```json
 {
     "language": "English"
 }
 ```

## Success Response

**Code** : `200 OK`

## Error Response

**Code** : `XXX ERROR` <br>
**Condition** :<br>
_why this happens if not obvious from response code_ <br>
**Content** : `{ "error" : "[Message]" }`


**Code:** `401 UNAUTHORIZED` <br>
**Content:** `{ "error" : "Log in" }`

## Notes

* delete this section if doesn't needed